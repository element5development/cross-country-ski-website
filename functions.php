<?php

/** Start the engine */

require_once( get_template_directory() . '/lib/init.php' );



/** Child theme (do not remove) */

define( 'CHILD_THEME_NAME', 'Backcountry Child Theme' );

define( 'CHILD_THEME_URL', 'http://www.studiopress.com/themes/backcountry' );



$content_width = apply_filters( 'content_width', 470, 400, 910 );



/** Add new image sizes */

add_image_size( 'home-bottom', 170, 90, TRUE );

add_image_size( 'home-middle', 265, 150, TRUE );

add_image_size( 'home-mini', 50, 50, TRUE );



/** Reposition the primary navigation */

//remove_action( 'genesis_after_header', 'genesis_do_nav' );

//add_action( 'genesis_before_header', 'genesis_do_nav' );





/** Customize the post info function */

add_filter( 'genesis_post_info', 'backcountry_post_info_filter' );

function backcountry_post_info_filter( $post_info ) {



    return '[post_date] by [post_author_posts_link] &middot; [post_comments] [post_edit]';



}



/** Customize the post meta function */

add_filter( 'genesis_post_meta', 'backcountry_post_meta_filter' );

function backcountry_post_meta_filter( $post_meta ) {

    

	return '[post_categories before="Filed Under: "] &middot; [post_tags before="Tagged: "]';



}



/** Add after post ad section */

add_action( 'genesis_after_post_content', 'backcountry_after_post_ad', 9 ); 

function backcountry_after_post_ad() {



    if ( is_single() && is_active_sidebar( 'after-post-ad' ) ) {

    	echo '<div class="after-post-ad">';

		dynamic_sidebar( 'after-post-ad' );

		echo '</div><!-- end .after-post-ad -->';

	}



}



/** Add support for 3-column footer widgets */

add_theme_support( 'genesis-footer-widgets', 4 );



/** Register widget areas */

genesis_register_sidebar( array(

	'id'			=> 'home-top',

	'name'			=> __( 'Home Top', 'backcountry' ),

	'description'	=> __( 'This is the home top section.', 'backcountry' ),

) );

genesis_register_sidebar( array(

	'id'			=> 'home-left',

	'name'			=> __( 'Home Left', 'backcountry' ),

	'description'	=> __( 'This is the home left section.', 'backcountry' ),

) );

genesis_register_sidebar( array(

	'id'			=> 'home-right',

	'name'			=> __( 'Home Right', 'backcountry' ),

	'description'	=> __( 'This is the home right section.', 'backcountry' ),

) );

genesis_register_sidebar( array(

	'id'			=> 'home-bottom',

	'name'			=> __( 'Home Bottom', 'backcountry' ),

	'description'	=> __( 'This is the home bottom section.', 'backcountry' ),

) );

genesis_register_sidebar( array(

	'id'			=> 'after-post-ad',

	'name'			=> __( 'After Post Ad', 'magazine' ),

	'description'	=> __( 'This is the after post ad section.', 'backcountry' ),

) );



add_action('get_header','child_remove_title');

function child_remove_title(){

	if (is_front_page())

		{remove_action('genesis_post_title', 'genesis_do_post_title');

	}

}



add_filter('genesis_footer_output', 'footer_output_filter', 10, 3);

function footer_output_filter($output) {

	$bottom_menu = wp_nav_menu( array('menu' => 'Footer Navigation' ));

	$output = $bottom_menu;

 	return $output;

} 



// Display 24 products per page. Goes in functions.php

add_filter( 'loop_shop_per_page', create_function( '$cols', 'return 24;' ), 20 );



// Change number or products per row to 3

add_filter('loop_shop_columns', 'loop_columns');

if (!function_exists('loop_columns')) {

	function loop_columns() {

		return 3; // 3 products per row

	}

}

add_filter("gform_pre_submission_110", "wdm_evaluate_results");




function wdm_evaluate_results($form)

{

    $total = 0;

    // add the values of selected results

    $total += $_POST["input_1"] ;
    $total += $_POST["input_3"] ;
    $total += $_POST["input_2"] ;

    // set the value of the hidden field

    $_POST["input_4"] = $total;

}

// Add theme support for Genesis
add_theme_support( 'genesis-connect-woocommerce' );

/** Reposition the primary navigation */
remove_action( 'genesis_after_header', 'genesis_do_nav' );
add_action( 'genesis_before_header', 'genesis_do_nav' );